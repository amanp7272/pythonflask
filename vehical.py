import random


class Vehical():

    def __init__(self):
        self.val = 0
        self.cost = 4100 #Setting the default value when object is created

    def increment_vehical(self):
        self.val += 1

    def type(self):
        print(self)
        print('I have a type')

        #Setting the Instance Attribute
        self.random_value = random.randint(1, 10)

#Instance Type
car = Vehical()
print("First Object value: ", car.val, car.cost)
car.increment_vehical()
print(car)
car.type()                      #Calling Instance Method
print(car.random_value)         #Calling the Instance attribute

bike = Vehical()
print("Second Object value: ", car.val, car.cost)
