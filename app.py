from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)

#DB Configuration
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///posts.db'
db = SQLAlchemy(app)

#Create Model For DB
class BlogPost(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    content = db.Column(db.Text, nullable=False)
    author = db.Column(db.String(20), nullable=False, default='N/A')
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    #printout New model every time
    def __repr__(self):
        return 'Blog Post ' + str(self.id)

#Data For Dynamic Front End
all_posts = [
    {
        'title':'Post 1',
        'content':'This is content of post 1',
        'author' : 'Aman'
    },
    {
        'title':'Post 2',
        'content':'This is content of post 2'
    }
]

#Render HTML Pages
@app.route('/')
def index():
    return render_template('index.html')

#Dynamic Front End, Pass Data to HTML Page and loop it
@app.route('/posts', methods=['GET'])
def posts():
   all_posts = BlogPost.query.order_by(BlogPost.date_posted).all()
   return render_template('posts.html', posts=all_posts)

@app.route('/posts/create', methods=['GET','POST'])
def create():
    if request.method == 'POST':
       post_title = request.form['title']
       post_content = request.form['content']
       post_author = request.form['author']
       new_post = BlogPost(title=post_title, content=post_content, author= post_author)
       db.session.add(new_post)
       db.session.commit()
       return redirect('/posts')
    else:
        return render_template('create.html')

@app.route('/posts/delete/<int:id>')
def delete(id):
    post = BlogPost.query.get_or_404(id)
    db.session.delete(post)
    db.session.commit()
    return redirect('/posts')

@app.route('/posts/edit/<int:id>', methods=['GET', 'POST'])
def Edit(id):
    post = BlogPost.query.get_or_404(id)
    if request.method == 'POST':
        post.title = request.form['title']
        post.author = request.form['content']
        post.content = request.form['author']
        db.session.commit()
        return redirect('/posts')
    else:
        return render_template('edit.html', posts=post)
    


#Dynamic URL Examples
@app.route('/home/<string:name>')
def hello(name):
    return "Hello world, " + name

@app.route('/home/<int:id>')
def hello2(id):
    return "Hello number, " + str(id)

@app.route('/home/<string:name>/<int:id>')
def hello3(name, id):
    return "Hello User, " + name + " Your id is, " + str(id)

#Restrict URL for perticular request methods
@app.route('/onlyget', methods=['GET'])
def get_req():
    return 'Only GET request Work'





if __name__ == "__main__":
    app.run(debug=True)